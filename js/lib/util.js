var gamejs = require('gamejs');
var Vector2d = require('lib/vector2d').Vector2d;

var pixels_to_meters = exports.pixels_to_meters = function(pixels) {
  return pixels * 0.1;
};

var meters_to_pixels = exports.meters_to_pixels = function(meters) {
  return meters * 10;
};

var closest_point = exports.closest_point = function(A, A_direction, B) {
  var dot = B.clone().subtract(A).dot(A_direction);
  return A_direction.clone().scale(dot).add(A);
};

var distance_to_line = exports.distance_to_line = function(A, A_direction, B) {
  return closest_point(A, A_direction, B).distanceTo(B);
};

var signed_distance_to_line = exports.signed_distance_to_line = function(A, A_direction, B) {
  var nearest_point = closest_point(A, A_direction, B);
  var distance = nearest_point.distanceTo(B);
  var sign = nearest_point.subtract(B).dot(A_direction.clone().normal()) > 0 ? 1 : -1;
  return distance * sign;
};

var distance_to_line_segment = exports.distance_to_line_segment = function(A1, A2, B) {
  var dir = A2.clone().subtract(A1).unit();

  // P == point on line, A1-A2, that is closest to point, B
  var P = closest_point(A1, dir, B);
  
  if (P.clone().subtract(A1).dot(dir) < 0) {
    P = A1;
  } else if (A2.clone().subtract(P).dot(dir) < 0) {
    P = A2;
  }

  return P.distanceTo(B);
};

var signed_distance_to_line_segment = exports.signed_distance_to_line_segment = function(A1, A2, B) {
  var dir = A2.clone().subtract(A1).unit();

  // P == point on line, A1-A2, that is closest to point, B
  var P = closest_point(A1, dir, B);
  
  if (P.clone().subtract(A1).dot(dir) < 0) {
    P = A1;
  } else if (A2.clone().subtract(P).dot(dir) < 0) {
    P = A2;
  }

  var sign = P.clone().subtract(B).dot(dir.normal()) > 0 ? 1 : -1;

  return P.distanceTo(B) * sign;
};

var line_line_intersection = exports.line_line_intersection = function(A, B, C, D) {
  var CmP = C.clone().subtract(A);
      r   = B.clone().subtract(A);
      s   = D.clone().subtract(C);
  
      CmPxr = CmP[0] * r[1] - CmP[1] * r[0],
      CmPxs = CmP[0] * s[1] - CmP[1] * s[0],
      rxs   = r[0] * s[1] - r[1] * s[0];
  
  if (CmPxr == 0) {
    // Lines are collinear, and so intersect if they have any overlap
    return ( ( C[0] - A[0] < 0 ) != ( C[0] - B[0] < 0 ) ) || 
           ( ( C[1] - A[1] < 0 ) != ( C[1] - B[1] < 0 ) );
  }
  
  // Lines are parallel
  if (rxs == 0) return false;
  
  var rxsr = 1 / rxs;
  var u    = CmPxr * rxsr;
  var t    = CmPxs * rxsr;
  
  if (t >= 0 && t <= 1 && u >= 0 && u <= 1) {
    return s.multiply(u).add(C);
  }
  
  return false;
};

var circle_line_intersection = exports.circle_line_intersection = function(point1, point2, circle, radius) {
  var p1 = point1.clone(),
      p2 = point2.clone(),
      dx = p2[0] - p1[0],
      dy = p2[1] - p1[1],
      length = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)),
      sign = (dx == 0 || dy == 0) ? 1 : (dx * dy) / Math.abs(dx * dy),
      angle_r = -Math.acos(Math.abs(dx) / length) * sign;

  p1.rotate(angle_r, circle);
  p2.rotate(angle_r, circle);

  var y = p1[1];

  // No intersection if the y-intercept is greater than the circle radius
  if (Math.abs(y - circle[1] - 0.0001) > radius) return false;

  /**
      Circle Equation
       (1)  (x - a)^2 + (y - b)^2 = r^2

      Solve for x
       (2)  x = a + Sqrt[ 2*b*y - (b*b) + (r*r) - (y*y) ]
       (3)  x = a - Sqrt[ 2*b*y - (b*b) + (r*r) - (y*y) ]
   */
  var a = circle[0],
      b = circle[1],
      r = radius,
      sqrt = Math.sqrt(2*b*y - (b*b) + (r*r) - (y*y));

  var x_plus = a + sqrt,
      x_minus = a - sqrt;

  return [
    (new Vector2d(x_plus, y)).rotate(-angle_r, circle),
    (new Vector2d(x_minus, y)).rotate(-angle_r, circle)
  ];
};

/**
 t = (-v +- Sqrt[ v^2 + 2ad ]) / 2
 */
var time_to_position = exports.time_to_position = window.time_to_position = function(d, v, a) {
  return (-v + Math.sqrt(v*v + 2 * a * d)) / a;
}

var find_closest_position = exports.find_closest_position = function(ball, player, point1, point2, cycle) {
  cycle = cycle || 0;

  function sort_points(a, b) {
    return ball.direction.dot(a) - ball.direction.dot(b);
  }

  var sorted_points = [point1, point2].sort(sort_points);
  var p1 = sorted_points[0]
      p2 = sorted_points[1];

  if (ball.direction.dot(p1.clone().subtract(ball.position)) < 0) p1 = ball.position;
  if (ball.direction.dot(p2.clone().subtract(ball.stop_position)) > 0) p2 = ball.stop_position;

  var p3 = p2.clone().subtract(p1).scale(0.5).add(p1);

  var d1 = ball.position.distanceTo(p3),
      d2 = player.position.distanceTo(p3);

  var t1 = time_to_position(pixels_to_meters(d1), ball.currentSpeed(), ball.deceleration),
      t2 = time_to_position(pixels_to_meters(d2), player.speed, player.deceleration);

  if (cycle >= 7) return [p3, d1, t1, t2];

  if (t1 > t2) {
    return find_closest_position(ball, player, p1, p3, cycle+1);
  } else {
    return find_closest_position(ball, player, p3, p2, cycle+1);
  }
}
