var gamejs = require('gamejs');
var Vector2d = require('lib/vector2d').Vector2d;
var Field = require('app/field').Field;
var Ball = require('app/ball').Ball;
var PlayerManager = require('app/player_manager').PlayerManager;
var Player = require('app/player').Player;
var Input = require('lib/input').Input;
var TeamManager = require('app/team').TeamManager;

// gamejs.preload([]);

var Target = function(color, radius) {
  this.position = [];
  this.radius = radius || 8;

  this.setRadius = function(r) {
    this.radius = r;
  }

  this.setPosition = function(x, y) {
    if (x[0] != undefined) {
      this.position[0] = x[0];
      this.position[1] = x[1];
    } else {
      this.position = [x, y];
    }
  }

  this.draw = function(display) {
    if (this.position.length) {
      gamejs.draw.circle(display, color, this.position, this.radius, 2);
    }
  }
};

window.Targets = (function() {
  var targets = [];

  return {
    add: function(color, size) {
      var t = new Target(color, size);
      targets.push(t);
      return t;
    },

    draw: function(display) {
      for (var i=0, len=targets.length; i<len; ++i) {
        targets[i].draw(display);
      }
    }
  };
})();




gamejs.ready(function() {

    var display = gamejs.display.setMode([1000, 500]);
    Field.initialize([1000, 500], Ball, PlayerManager);
    Field.reset();

    TeamManager.initialize();

    var dt;

    function tick(msDuration) {
      dt = msDuration * 0.001;

      display.clear();

      Ball.update(dt);
      PlayerManager.update(dt);

      Field.resolveCollisions(dt);

      Ball.draw(display);
      PlayerManager.draw(display);
      Field.draw(display);

      Targets.draw(display);

      // game loop
      return;
    };

    gamejs.time.interval(tick);
});

$(document).ready(function(){

  $('#gjs-canvas').on('click', function(e){

    var offX, offY;
    if (!(e.offsetX || e.offsetY)) {
      offX = e.pageX - $(e.target).position().left;
      offY = e.pageY - $(e.target).position().top;
    }
    else {
      offX = e.offsetX;
      offY = e.offsetY;
    }

    Ball.shootAtTarget(new Vector2d(offX, offY));
    //player.strike(Ball);

  });

});
