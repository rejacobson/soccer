var gamejs = require('gamejs');
var Vector2d = require('lib/vector2d').Vector2d;
var util = require('lib/util');
var Class = require('lib/inheritance').Class;
var Entity = require('app/entity').Entity;

var Player = exports.Player = Class.extend(Entity, {
  initialize: function(color) {
    this.color = color;

    this.mass = 5;
    this.radius = 4;
    this.parent();
    this.setFriction(5);
    this.speed = 40;

    this.Dradius = Targets.add('#ff00ff');
    this.ip1 = Targets.add('#00ffff');
    this.ip2 = Targets.add('#00ffff');

    this.kicked_ball = false;
  },

  draw: function(display) {
    gamejs.draw.circle(display, this.color, this.position, this.radius);  
  },

  kick: function(target) {
    if (this.kicked_ball) return;
    target.addForce(this.direction.clone().scale(15));
    if (target.direction.dot(this.velocity) < 0) target.setVelocity(target.velocity.reflect(this.direction));
    this.kicked_ball = true;
  },

  update: function(dt) {
    this.parent(dt);

    if (this.velocity.lengthOfSq() <= 0) this.kicked_ball = false;
  },

  /**
    Distance Traveled
      d = -v^2 / 2a

   */
  intercept: function(target) {
    if (!target.isMoving()) {
      return this.strike(target.position.clone());
    }
  
    var origin = this.position,
        d = -(this.speed*this.speed) / (2 * this.deceleration);

    this.Dradius.setPosition(this.position);
    this.Dradius.setRadius(util.meters_to_pixels(d));

    // 1. Is the player actually within range of the ball?
    var dist_to_point = util.distance_to_line(target.position, target.direction, this.position);

    // Not within range - aim for the balls stop position
    if (dist_to_point > util.meters_to_pixels(d)) {
      return this.strike(target.stop_position.clone());
    }

    // 2. Within range.  Figure out the intersection with the ball's line and the player's range radius
    var intersect_points = util.circle_line_intersection(target.position, target.stop_position, this.position, util.meters_to_pixels(d));

    this.ip1.setPosition(intersect_points[0]);
    this.ip2.setPosition(intersect_points[1]);

    var result = util.find_closest_position(target, this, intersect_points[0], intersect_points[1]);

    this.strike(result[0]);
  },

  strike: function(position) {
    var v = position.subtract(this.position).unit();
    this.setVelocity(v.scale(this.speed));
  }
});
