var gamejs = require('gamejs');
var Vector2d = require('lib/vector2d').Vector2d;
var util = require('lib/util');


var LineSegment = function(p1, p2) {
  // Points
  this.P1 = p1;
  this.P2 = p2;

  this.C = this.P2.clone().subtract(this.P1).scale(0.5).add(this.P1);

  // Vectors
  this.segment = p2.clone().subtract(p1);
  this.direction = this.segment.clone().unit();
  this.normal = this.direction.clone().normal();

  // Scalars
  this.length = this.segment.lengthOf();
};

var Net = function(position, size) {
  var _rect = new gamejs.Rect(position, size);

  this.isInside = function(point) {
    return _rect.collidePoint(point);
  }

  this.rect = function() {
    return _rect;
  }
};

Net.size = function() {
  return [50, 150];
};


var Field = exports.Field = (function() {
  var _bounds,
      _walls,
      _nets,
      _ball,
      _player_manager,
      _positions = {
        0: [],
        1: []
      };

  // Reset the ball and players to their starting positions
  var _reset = function() {
    _ball.setPosition(_bounds.center.slice(0));
    _ball.velocity.set(0, 0);
  }

  // quick test to see if an entity is out of bounds
  var _out_of_bounds = function(entity) {
    return entity.position[0] - entity.radius < _bounds.left ||
           entity.position[0] + entity.radius > _bounds.right ||
           entity.position[1] - entity.radius < _bounds.top ||
           entity.position[1] + entity.radius > _bounds.bottom; 
  }

  // check if the ball is in a net
  var _goal_scored = function() {
    _.each(_nets, function(net, type) {
      if (net.isInside(_ball.position)) {
console.log('GOAL!!!!!! ---------- '+ type);
        _reset();
      }
    });
  };

  // collisions between players and the ball
  var _resolve_entity_collision = function(player) {
    if (_ball.position.distanceToSq(player.position) > Math.pow(player.radius + _ball.radius, 2)) return;
    player.kick(_ball);
  };

  // collisions between the ball, players and field walls
  var _wallcache;
  var _resolve_wall_collision = function(entity) {
    if (!_out_of_bounds(entity)) return false;  

    _wallcache = [];

    _.each(_walls, function(line, index) {
      if (entity.direction.dot(line.normal) < 0) return;  
      var distance_to_wall = util.signed_distance_to_line_segment(line.P1, line.P2, entity.position) - entity.radius;   
      _wallcache.push([line, distance_to_wall]);
    });

    // sort; closest wall first
    _wallcache.sort(function(a, b){
      return a[1] - b[1];
    });
     
    var recalculate = false;
    _.each(_wallcache, function(wall, name) {
      var line = wall[0],
          dist = recalculate ? (util.signed_distance_to_line_segment(line.P1, line.P2, entity.position) - entity.radius) : wall[1]; 

      // Only negative distance is penetrating the wall
      if (dist > 0) return;
       
      entity.setVelocity(entity.velocity.reflect(line.normal));
      recalculate = true; 
    });
     
/*
    // Check right wall
    if (entity.velocity.x > 0) {
      var distance_to_wall = util.distance_to_line(this.walls.right[0], Vector2d([0, -1]), entity.position) - entity.radius;   
      var dp = Vector2d([distance_to_wall, distance_to_wall * entity.direction.y / entity.x]);
      var new_point = entity.position.clone().add(dp);

      var l = db.lengthOf();

      entity.setVelocity(entity.velocity.reflect([1, 0]));
      entity.setPosition(entity.direction.clone().scale(l).add(new_point));

    // Check left wall
    } else {
    }
*/
  }

  return {
    getBall: function() {
      return _ball;
    },

    getPositions: function(n) {
      if (n !== undefined && _positions[n]) return _positions[n];
      return _positions;
    },

    initialize: function(size, ball, player_manager) {
      _ball = ball;
      _player_manager = player_manager;

      var wall_segment_height = (size[1] - Net.size()[1]) / 2;

      _bounds = {
        top: 0,
        right: size[0] - Net.size()[0],
        bottom: size[1],
        left: Net.size()[0],
        center: [size[0]/2, size[1]/2]
      };

      _walls = {
        //  |
        //  x------->
        top: new LineSegment(Vector2d(_bounds.left, _bounds.top), Vector2d(_bounds.right, _bounds.top)),

        //  x-
        //  |
        //  |
        //  v 
        //right: new LineSegment(Vector2d(_bounds.right, _bounds.top), Vector2d(_bounds.right, _bounds.bottom)),
        righttop: new LineSegment(Vector2d(_bounds.right, _bounds.top), Vector2d(_bounds.right, _bounds.top + wall_segment_height)),
        rightbottom: new LineSegment(Vector2d(_bounds.right, _bounds.bottom - wall_segment_height), Vector2d(_bounds.right, _bounds.bottom)),

        rightnettop: new LineSegment(Vector2d(_bounds.right, _bounds.top + wall_segment_height), Vector2d(size[0], _bounds.top + wall_segment_height)),
        rightnetside: new LineSegment(Vector2d(size[0], _bounds.top + wall_segment_height), Vector2d(size[0], _bounds.bottom - wall_segment_height)),
        rightnetbottom: new LineSegment(Vector2d(size[0], _bounds.bottom - wall_segment_height), Vector2d(_bounds.right, _bounds.bottom - wall_segment_height)),

        //  <-------x
        //          |
        bottom: new LineSegment(Vector2d(_bounds.right, _bounds.bottom), Vector2d(_bounds.left, _bounds.bottom)),

        //  ^
        //  |
        //  |
        // -x 
        //left: new LineSegment(Vector2d(_bounds.left, _bounds.bottom), Vector2d(_bounds.left, _bounds.top)),
        lefttop: new LineSegment(Vector2d(_bounds.left, _bounds.top + wall_segment_height), Vector2d(_bounds.left, _bounds.top)),
        leftbottom: new LineSegment(Vector2d(_bounds.left, _bounds.bottom), Vector2d(_bounds.left, _bounds.bottom - wall_segment_height)),

        leftnettop: new LineSegment(Vector2d(0, _bounds.top + wall_segment_height), Vector2d(_bounds.left, _bounds.top + wall_segment_height)),
        leftnetside: new LineSegment(Vector2d(0, _bounds.bottom - wall_segment_height), Vector2d(0, _bounds.top + wall_segment_height)),
        leftnetbottom: new LineSegment(Vector2d(_bounds.left, _bounds.bottom - wall_segment_height), Vector2d(0, _bounds.bottom - wall_segment_height)),
      }; 

      _nets = {
        left: new Net([0, _bounds.top + wall_segment_height], [Net.size()[0] - _ball.radius, Net.size()[1]]),
        right: new Net([_bounds.right + _ball.radius, _bounds.top + wall_segment_height], [Net.size()[0] - _ball.radius, Net.size()[1]])
      };

      // Set player positions according to the field dimensions.
      var cx = _bounds.center[0],
          cy = _bounds.center[1];

      _positions[0] = [[cx-100, cy-80], [cx-120, cy], [cx-100, cy+80], [cx-320, cy-150], [cx-290, cy], [cx-320, cy+150], [_bounds.left+20, cy]];
      _positions[1] = [[cx+100, cy-80], [cx+120, cy], [cx+100, cy+80], [cx+320, cy-150], [cx+290, cy], [cx+320, cy+150], [_bounds.right-20, cy]];
    },

    resolveCollisions: function() {
      // 1. Resolve collisions with the walls.
      if (_ball.isMoving()) _resolve_wall_collision(_ball);

      _player_manager.each(function(player) {
        if (player.isMoving()) _resolve_wall_collision(player);
      });

      // 2. Resolve collisions between the ball and players
      _player_manager.each(function(player) {
        _resolve_entity_collision(player);
      });
      
      // 3. Check for a goal
      _goal_scored();
    },

    draw: function(display) {
      _.each(_walls, function(wall) {
        gamejs.draw.line(display, '#9999ff', wall.P1, wall.P2, 2);
        gamejs.draw.line(display, '#00ff00', wall.C, wall.C.clone().add(wall.normal.clone().reverse().scale(8)), 1); 
      });

      _.each(_nets, function(net) {
        gamejs.draw.rect(display, '#aa0000', net.rect(), 2);
      });

      _.each(_positions[0], function(point) {
        gamejs.draw.circle(display, '#555555', point, 8, 1);
      });

      _.each(_positions[1], function(point) {
        gamejs.draw.circle(display, '#555555', point, 8, 1);
      });
    },

    reset: _reset
  };

})();


