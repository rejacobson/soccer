var gamejs = require('gamejs');
var Vector2d = require('lib/vector2d').Vector2d;
console.log('Start of Entity');
console.log(Vector2d);
var util = require('lib/util');
var Class = require('lib/inheritance').Class;

console.log(Vector2d);

var Entity = exports.Entity = Class.extend({
  initialize: function() {
    // Mass
    this.mass = 1;
    this.radius = 5;

    this.friction;
    this.weight;
    this.deceleration;

    // Coefficient of friction
    this.setFriction(0.8);
    
    this.position = new Vector2d(0, 0);
    this.last_position = new Vector2d(0, 0);
    this.velocity = new Vector2d(0, 0);
    this.force = new Vector2d(0, 0);
    
    this.stop_position = new Vector2d(0, 0);
    this.direction = new Vector2d(0, 0);
  },

  isMoving: function() {
    return this.velocity.lengthOfSq() > 0;
  },

  currentSpeed: function() {
    return this.velocity.lengthOf();
  },

  setFriction: function(u) {
    this.friction = u;
    this.deceleration = -(9.8 * this.friction);
  },

  setVelocity: function(v) {
    var distance = -Math.pow(v.lengthOf(), 2) / (2 * this.deceleration);

    this.velocity.set(v);
    this.direction = this.velocity.clone().unit();
    this.stop_position = this.direction.clone().scale(util.meters_to_pixels(distance)).add(this.position);
  },

  addForce: function(v) {
    this.force.add(v);
  },

  setPosition: function(p) {
    this.position.set(p);
  },

  update: function(dt) {
    if (this.force.lengthOfSq() > 0) {
      this.setVelocity(this.velocity.add(this.force));
      this.force.set(0, 0);
    }

    if (this.velocity.lengthOf() > 0.5 ) {  
      var accel = this.direction.clone().scale(this.deceleration);
      this.velocity.add([accel[0] * dt,
                         accel[1] * dt]);

      if (this.velocity.dot(this.direction) < 0) this.velocity.set(0, 0);
    } else {
      this.velocity.set(0, 0);
    }

    this.last_position.set(this.position);

    this.position.add([util.meters_to_pixels(this.velocity[0] * dt),
                       util.meters_to_pixels(this.velocity[1] * dt)]);
  }
  
});

