var gamejs = require('gamejs');
var Vector2d = require('lib/vector2d').Vector2d;
var util = require('lib/util');
var Class = require('lib/inheritance').Class;
var Entity = require('app/entity').Entity;


var Ball = Class.extend(Entity, {
  initialize: function(){
    this.parent();
    this.radius = 6;
  },

  /**

    Final Velocity
         vf = vi + a t 
          0 = vi + a t     // vf is 0
        a t = -vi
     (1)  t = -vi / a

    Distance Traveled
          d = (vi + vf) t / 2
          d = vi t / 2          // vf is 0
     (2)  t = 2 d / vi 

    Initial Velocity needed to stop at target
          (1) = (2)
          -vi / a = 2 d / vi
     (3)  vi = +- Sqrt[2da]

   */
  shootAtTarget: function(target) {
/*
    var dir = target.subtract(this.position).unit();
    this.setVelocity(dir.scale(45)); 
*/
    // Find the exact velocity needed to exactly arrive at the target
    var d = util.pixels_to_meters(this.position.distanceTo(target)),
        v = target.clone().subtract(this.position).unit(),
        velocity = Math.sqrt(2 * d * Math.abs(this.deceleration));

    this.setVelocity(v.scale(velocity));

    if (!this.target) this.target = Targets.add('#ff0000');
    this.target.setPosition(this.stop_position);
  },

  draw: function(display) {
    gamejs.draw.circle(display, '#ff0000', this.position, this.radius);  
  }
});

exports.Ball = new Ball();
