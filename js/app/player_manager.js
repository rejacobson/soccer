var PlayerManager = exports.PlayerManager = (function(){

  var players = [];

  return {
    add: function(player) {
      players.push(player);
      return player;
    },

    each: function(callback) {
      for (var i=0, len=players.length; i<len; ++i) {
        callback(players[i]);
      }
    },

    update: function(dt) {
      for (var i=0, len=players.length; i<len; ++i) {
        players[i].update(dt);
      }
    },

    draw: function(display) {
      for (var i=0, len=players.length; i<len; ++i) {
        players[i].draw(display);
      }
    }
  };

})();
