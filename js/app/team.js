var gamejs = require('gamejs');
var Input = require('lib/input').Input;
var PlayerManager = require('app/player_manager').PlayerManager;
var Player = require('app/player').Player;
var Field = require('app/field').Field;

var NUMPLAYERS = 3;

var KEYS = {
  0: ['e', 'd', 'c', 'w', 's', 'x', 'space'],
  1: ['kp7', 'kp4', 'kp1', 'kp8', 'kp5', 'kp2', 'kp0']
};

var Team = function(number, color) {
  this.players = [];

  function keypress_callback() {
    this.intercept(Field.getBall());
  }

  var positions = Field.getPositions(number);

  for (var i=0; i<NUMPLAYERS; ++i) {
    var player = new Player(color);
    PlayerManager.add(player);
    this.players.push(player) 

    player.setPosition(positions[i]);
    Input.on('keydown.'+ KEYS[number][i], keypress_callback.bind(player));
  }
  
};

// Move team members back to their starting positions
Team.prototype.reset = function() {

};


var TeamManager = exports.TeamManager = (function() {
  var _teams;

  return {
    initialize: function() {
      _teams = [
        new Team(0, '#00ff00'),
        new Team(1, '#0000ff')
      ];
    }
  };
})();

